extends Node2D

export var speed = 4

var direction = Vector2(0, 1)

func _ready(): 
	randomize()
	var r = rand_range(0, 1)
	var g = rand_range(0, 1)
	var b = rand_range(0, 1)
	$Body.modulate = Color(r, g, b, 1)

func _process(delta):
	if Input.is_action_pressed("go_left") and not direction == Vector2.RIGHT:
		direction = Vector2.LEFT
	elif Input.is_action_pressed("go_right") and not direction == Vector2.LEFT:
		direction = Vector2.RIGHT
	elif Input.is_action_pressed("go_up") and not direction == Vector2.DOWN:
		direction = Vector2.UP
	elif Input.is_action_pressed("go_down") and not direction == Vector2.UP:
		direction = Vector2.DOWN
	position += direction * speed

func set_direction(new_direction: Vector2):
	direction = new_direction

func _on_Area2D_body_entered(body):
	queue_free()
	get_tree().change_scene("res://scenes/Menu.tscn")
