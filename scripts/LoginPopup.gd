extends PopupDialog

func _on_LoginButton_pressed():
	var new_account = $CheckBox.pressed
	var username = null
	
	if new_account:
		username = $LoginUsername.text

	var email = $LoginEmail.text
	var password = $LoginPassword.text
	
	var session = yield(State.client.authenticate_email_async(email, password, username, new_account), "completed")
	
	if session.is_exception():
		print(session.get_exception())
		return

	State.session = session
	State.is_logged_in = true
	
	yield(State.socket.connect_async(session), "completed")
	get_parent().emit_signal("username_changed", session.username)
	hide()


func _on_LoginPopup_about_to_show():

	if State.is_logged_in:
		$LogoutButton.show()
		$LoginEmail.hide()
		$LoginEmailLabel.hide()
		$LoginPassword.hide()
		$LoginPasswordLabel.hide()
		$LoginButton.hide()
		$CheckBox.hide()
		$LoginUsername.hide()
		$LoginUsernameLabel.hide()
	else:
		$LogoutButton.hide()
		$LoginEmail.show()
		$LoginEmailLabel.show()
		$LoginPassword.show()
		$LoginPasswordLabel.show()
		$LoginButton.show()
		$CheckBox.show()
