extends PopupDialog

var matchItems := []

func _ready():
	pass

func _on_MatchList_about_to_show():
	$ItemList.clear()
	var re = yield(State.socket.rpc_async("find_matches_rpc"), "completed")
	print("RPC result %s" % re)

	matchItems.append(re.payload)
	$ItemList.add_item(re.payload)
	$ItemList.get_selected_items()


func _on_MatchList_popup_hide():
	pass

func _on_JoinButton_pressed():
	var match_id = matchItems[$ItemList.get_selected_items()[0]]
	State.joined_match = match_id
	var match_data = yield(State.socket.join_match_async(
		match_id
	), "completed")
	hide()
	print("MATCH DATA BELOW:")
	print(match_data)
	get_parent().get_node("Lobby").show_other_players(match_data.presences)
	get_parent().get_node("Lobby").show()
