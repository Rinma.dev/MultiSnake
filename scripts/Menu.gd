extends Node

signal username_changed
var nk_match

func _ready():
	connect("username_changed", self, "_on_Username_Changed")

func _on_JoinGame_pressed():
	$MatchList.popup()

func _on_CloseMatchList_pressed():
	$MatchList.hide()

func _on_HostGame_pressed():
	nk_match = yield(
	  State.socket.create_match_async(),
	  "completed"
	)
	State.joined_match = nk_match.match_id
	$Lobby.popup()

func _on_LobbyLeave_pressed():
	$Lobby.hide()

func _on_LobbyStart_pressed():
	yield(State.socket.send_match_state_async(State.joined_match, 1, JSON.print({"game": "start"})), "completed")
	get_tree().change_scene("res://scenes/Game.tscn")

func _on_LoginButton_pressed():
	$LoginPopup.popup()

func _on_LoginClose_pressed():
	$LoginPopup.hide()

func _on_CheckBox_pressed():
	if $LoginPopup/CheckBox.pressed:
		$LoginPopup/LoginUsernameLabel.show()
		$LoginPopup/LoginUsername.show()
	else:
		$LoginPopup/LoginUsernameLabel.hide()
		$LoginPopup/LoginUsername.hide()

func _on_Username_Changed(username: String):
	$LoginButton.text = username

func _on_LogoutButton_pressed():
	State.session = null
	State.is_logged_in = false
	$LoginButton.text = "Login"
	$LoginPopup.hide()
