extends Node

onready var client : NakamaClient = Nakama.create_client(
	"2Z3THX3ZaAnt5BSZAoKRIyGdOUNrSrIzutjPaovjIeqFbrjS4l2jeWaK4qqkbdaa", 
	"gameserver-marvin.over-world.org", 
	443, 
	"https"
)
onready var socket := Nakama.create_socket_from(client)

var session : NakamaSession
var is_logged_in : bool
var joined_match := ""

func _ready():
	is_logged_in = false
	socket.connect("closed", self, "_on_socket_closed")
	socket.connect("connected", self, "_on_socket_connected")
	
func _on_socket_closed():
	socket = null
	print("Socket closed")

func _on_socket_connected():
	print("Socket connected")
