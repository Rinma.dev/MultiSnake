extends PopupDialog

var players_in_lobby := []

func _ready():
	State.socket.connect("received_match_presence", self, "_on_match_presence")
	State.socket.connect("received_match_state", self, "_on_match_state")

func _on_Lobby_about_to_show():
	pass

func _on_Lobby_popup_hide():
	$ItemList.clear()
	pass

func _on_User_Joined_Lobby():
	pass

func show_other_players(other_players):
	$ItemList.clear()
	for user in other_players:
		players_in_lobby.append(user.username)
		$ItemList.add_item(user.username);

func _on_match_presence(p_presence):
	print(p_presence)
	for user in p_presence.joins:
		players_in_lobby.append(user.username)
		$ItemList.add_item(user.username);
	
	for user in p_presence.leaves:
		players_in_lobby.remove(players_in_lobby.find(user.username))
		$ItemList.clear()
		for p in players_in_lobby:
			$ItemList.add_item(p);

func _on_LobbyLeave_pressed():
	yield(State.socket.leave_match_async(State.joined_match), "completed")

func _on_match_state(p_state: NakamaRTAPI.MatchData):
	print("Received match state with opcode %s, data %s" % [p_state.op_code, parse_json(p_state.data)])
	var code = p_state.op_code
	var data = parse_json(p_state.data)

	match code:
		1:
			if data.game == "start":
				get_tree().change_scene("res://scenes/Game.tscn")
	